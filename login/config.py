"""Config flask app
"""

import os

BASEDIR = os.getcwd()

TESTING = False

DEBUG = True

SECRET_KEY = "secretkey"

JWT_ALGORITHMS = "HS256"

SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASEDIR, "db.sqlite")

SQLALCHEMY_TRACK_MODIFICATIONS = True

REDIS_HOST = "localhost"

REDIS_PORT = 6379
