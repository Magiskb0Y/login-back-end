"""Login Project
"""

from flask import Flask
from flask import Blueprint
from werkzeug.utils import import_string
from flask_cors import CORS
from . import models
from . import decorators
from . import exceptions
from . import cache
from . import config
from .resources import user_api, admin_api


def create_app(config_path):
    """Function factory for application
    Parameters:
    -----------
    config_path: str, relative path for config file, base is working directory

    Return:
    -------
    application: object, WSGI object
    """
    config_object = import_string(config_path)
    application = Flask(__name__)
    application.config.from_object(config_object)

    user_bp = Blueprint("user_bp", __name__)
    user_api.init_app(user_bp)
    admin_bp = Blueprint("admin_bp", __name__)
    admin_api.init_app(admin_bp)

    application.register_blueprint(user_bp)
    application.register_blueprint(admin_bp, url_prefix="/admin")

    models.db.init_app(application)

    CORS(application)

    return application
