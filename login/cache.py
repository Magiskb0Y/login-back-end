"""Cache
"""

import redis
from login import config


store = redis.Redis(
    host=config.REDIS_HOST,
    port=config.REDIS_PORT,
    db=0
)
