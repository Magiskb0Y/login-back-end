import requests
import secrets

SERVER = "http://localhost:5000"
LOGIN_URL = "/session"
LOGOUT_URL = "/session"
REGISTER_URL = "/user"
GET_URL = "/user"
SET_PASSWORD_URL = "/user"
RESET_PASSWORD_URL = "/password/reset"


def test_register():
    data = {
        "username": secrets.token_urlsafe(8),
        "email": f"{secrets.token_urlsafe(8)}@gmail.com",
        "password": secrets.token_urlsafe(8)
    }

    url = SERVER + REGISTER_URL
    res = requests.post(url, json=data)
    assert res.status_code == 201


def test_login():
    data = {
        "username": "test",
        "password": "123456"
    }

    url = SERVER + LOGIN_URL
    res = requests.post(url, json=data)
    assert res.status_code == 201


def test_logout():
    res = requests.post(
        SERVER + LOGIN_URL,
        json={"username": "test", "password": "123456"}
    )
    access_token = res.json()["access_token"]
    url = SERVER + LOGOUT_URL
    res = requests.delete(url, headers={"Authorization": f"Bearer {access_token}"})
    assert res.status_code == 201


def test_set_password():
    res = requests.post(
        SERVER + LOGIN_URL,
        json={"username": "test", "password": "123456"}
    )
    access_token = res.json()["access_token"]
    url = SERVER + SET_PASSWORD_URL
    res = requests.put(url, json={"current_password": "123456", "new_password": "123456"}, headers={"Authorization": f"Bearer {access_token}"})
    assert res.status_code == 201


def test_get_user():
    res = requests.post(
        SERVER + LOGIN_URL,
        json={"username": "test", "password": "123456"}
    )
    access_token = res.json()["access_token"]
    url = SERVER + SET_PASSWORD_URL
    res = requests.get(url, headers={"Authorization": f"Bearer {access_token}"})
    assert res.status_code == 201


def test_reset_password():
    pass
