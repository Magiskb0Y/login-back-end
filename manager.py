"""Development utility for app
"""

import os
import shutil
import urllib
from flask import url_for
from flask_script import Manager, Shell, Command
from flask_migrate import Migrate, MigrateCommand
from login import create_app
from login import models


application = create_app("login.config")
manager = Manager(application)
migrate = Migrate(application, models.db)
manager.add_command("db", MigrateCommand)


def _make_context():
    """Make context for shell session in manager utility
    """
    global application
    return dict(application=application, db=models.db, models=models)


class ListRoute(Command):
    """list route function
    """
    def run(self):
        """Main function
        """
        global application
        output = []
        header = f"{'Endpoint':50s} {'Methods':20s} URL"
        for rule in application.url_map.iter_rules():
            options = {}
            for arg in rule.arguments:
                options[arg] = f"[{arg}]"
            methods = ','.join(rule.methods)
            url = url_for(rule.endpoint, **options)
            line = urllib.parse.unquote(
                f"{rule.endpoint:50s} {methods:20s} {url}")
            output.append(line)

        print(header)
        for line in sorted(output):
            print(line)


manager.add_command("route", ListRoute)
manager.add_command("shell", Shell(
    make_context=_make_context,
    use_ipython=True
))

if __name__ == "__main__":
    manager.run()
